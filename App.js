/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect,
  useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Platform,
  Dimensions,
  PermissionsAndroid,
  
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import MapView, {Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

const {height,width} = Dimensions.get('screen');

export default function App(){

  const [region, setRegion] = useState(null);
  const [markers, setMarkers] = useState([])

  useEffect (() => {
    getMyLocation()
  }, [])


  function getMyLocation(){
    Geolocation.getCurrentPosition(info => {
      console.log("LAT ", info.coords.latitude)
      console.log("LONG ", info.coords.longitude)

      setRegion({
        latitude:info.coords.latitude,
        longitude: info.coords.longitude,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      })

    },
    () => { console.log("deu algum erro")}, {
      enableHighAccuracy: true,
      timeout: 2000,
    })
  }

  function newMarker(event){
  

    let dados = {
      key: markers.length,
      coords:{
        latitude: event.nativeEvent.coordinate.latitude,
        longitude: event.nativeEvent.coordinate.longitude
      },
      pinColor: '#f1f2'
    }

    setRegion({
      latitude: event.nativeEvent.coordinate.latitude,
      longitude: event.nativeEvent.coordinate.longitude,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421
    })

    setMarkers(oldArray => [...oldArray, dados])

  }




  return(
    <View style = {styles.sectionContainer}>
      <MapView
       onMapReady={
          () => {
            Platform.OS === 'android' ?
            PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
              .then(() => {
                console.log("USUARIO ACEITOU")
              })
              : ''
          }}
          
       style={{width:width, height:height}}
       region={region}
       zoomEnabled={true}
       minZoomLevel={18}
       showsUserLocation={true}
       LoadingEnabled={true}
       onPress={(event) => newMarker(event)}
     >

          {markers.map(marker => {
            return(
              <Marker key={marker.key} coordinate={marker.coords} pinColor={marker.pinColor} />
            )
            
          })}

     </MapView>
     
    </View>
  )
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});
